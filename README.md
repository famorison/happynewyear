# HappyNewYear


## Getting started

To make it easy for you to get started with this New Year, here's a list of recommended next steps.


## Integrate with your tools

This year we will push more tools into production. There are many challenge to take. We will increase our knowledge and competences.


## Collaborate with your team

Those new challenge are exciting but they can also be stressful. Never forget that you're not alone.
My wish is that with fun and passion we can build our skills together and build a strong and effective team.

Alone we go faster, together we go further...

## Test and Deploy

On the 3rd of January, we will test and deploy the content of the box you received! ;-)


## Make sure you're staying safe

Since 2 years we have had difficult moments. This is probably not finished and we need to stay safe and patient. As soon as possible we will organize something to meet. Until then, stay safe and protect yourself and your loved ones.


## Project status
I wish you all the best for this year 2022. I hope it will be a great year for each of us.
Together we will complete this project and write the Ansible playbook for the New Year...

# Happy New Year 2022!
Frédérick

